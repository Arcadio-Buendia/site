
```ad-question
collapse: none
Let $\vec r$ be the position vector of a point on a closed contour C. What is the value of the line integral $\oint\vec r\cdot \vec dr$ ? 
```

## Understanding the Question:
  ![[Jest 2019, Section A, Question 1,fig1.excalidraw|300|left]]

$\vec r$ is the position vector for the closed contour C. 

## Designing the answer

#### Possible Methods:
 - Line integral by seperating $\vec r$ into it's components.[^1]
	 - There will be two variables since no obvious relation exists between x and y
-  [[contour integral]] can be used by treating $\vec r=f(z)$ and invoking [[contour integral#Cauchy's Theorem|cauchy's theorem]][^2]

#### Weapon of choice:
   - ![[contour integral#Cauchy's Theorem]]
	Since our position vector is [[analytic function|analytic]] inside and on the closed contour, we can use cauchy's theorem.
	
```ad-solution
$\oint \vec r.\vec dr$ is of the same form as $(i)$.
	Therefore,
$$\large \boxed{\oint \vec r.\vec dr=0}$$
```


###### footnotes
[^1]: Using the property that $\nabla \times \vec r=0\implies \oint \vec r\cdot d\vec r=0$([[Fundamental Theorems of Vector Analysis#Stoke's Theorem|Stoke's Theorem]]) is probably a better idea.
[^2]: Does $\vec r$ exist in the complex plane? Is it analytic on an inside C? 








