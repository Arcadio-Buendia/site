```ad-question
collapse: none
Q. A dc voltage of 80 Volt is switched on across a circuit containing a resistance of 5$\Omega$ in series with an inductance of 20 H . What is the rate of change of current at the instant when the current is 12 A?
```
#### Understanding the Question:
##### Circuit diagram
![[Jest 2019, Section A, Question 2.excalidraw]]

#### Designing an answer
We can use $V=IR$ but we also have inductance so we modify it for the [[inductance]] term as:
$$V=L\frac{dI}{dt}+IR$$
$$\boxed{\implies \frac{dI}{dt}=\frac{V-IR}{L}}$$

```ad-solution
$$\frac{dI}{dt}=\frac{80-12*5}{20}=1As^{-1}$$
```