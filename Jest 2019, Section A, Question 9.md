Q. A very long solenoid(axis along z direction) of n turns per unit length carries a current which increases linearly with time, $i=Kt$. What is the magnetic filed inside the solenoid at a given time t?

$a) B=\mu_0nKt\hat z \hspace{8cm}  b)B=\mu_0nK\hat Z$

$c) B=\mu_0nKt(\hat x+\hat y) \hspace{6.7cm} d)B=\mu_0cnKt\hat z \hspace{3cm}$

#### Understanding the Quesion
 ![[magnetic field of solenoid.excalidraw]]
  - 
#### Designing an answer
 - We could use Ampere's circuital law to get the answer
 - We can also use ![[magnetic field and current#Biot-Savart's law]] to narrow down our answer to the options.
#### solution
 - We now have $B\propto I\implies B \propto Kt$
 - $B$'s direction is given by cross product: $\vec I\times\hat r$'
 - The direction of $B$ from diagram is seen to be perpendicular to the $x-y$ plane. That is it's direction is $\hat z$
 - Since integration over loops shouldn't add a multiplicative constant
 - Our only option is $\boxed{a)B=\mu_0nKt\hat z}$