### Consider the function $f(x,y)=|x|-i|y|$. In which domain of the complex plane is it analytic?

#### Understanding the Question:
##### Complex Plane
![[complex plane.excalidraw]]

#####
We want to know the quadrant for which the given function is [[analytic function|analytic]].

#### Designing an answer
- We could check if the derivative of the above function exists in each quadrant by using limits.
- But ![[analytic function#Cauchy Reimann Condition|cauchy reimann equations]] provide an easier way.
#### Solution
 - $u=|x|$
 - $v=- |y|$
![[modulus graph.excalidraw]]

Quadrant|$\frac{\partial u}{\partial x}$|$\frac{\partial v}{\partial x}$
-------|--------------------------------|------------------------------
I|1| -1
II| -1 | -1
III| -1 | 1
IV| 1 | 1

The second condition of CR equation is always satisfied at 0.

Therefore the function is analytic at $\boxed{\large second \ and\ third\ quadrant}$