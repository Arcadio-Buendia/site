Q. White light of intensity $I_0$ is incident normally one filter plate of thickness d. The plate has a wavelength dependent absorption coefficient $a(\lambda)=a_o\left(1-\frac{\lambda}{\lambda_0}\right)$ per unit length. The band pass edge of the filter is defined as the wavelength at which the intensity, after passing through the filter is $I=\frac{I_0}{\rho},a_o,\lambda_o$ and $\rho$ are constants. the reflection coefficient of the plate may be assumed to be independent of $\lambda$. Which one of the following statements is true about the bandwidth of the filter?
a) The bandwidth is linearly dependent on $\lambda_0$
b) The bandwidth is independent of the plate thickness d
c) The bandwidth is linearly dependent on $a_0$
a) The bandwidth is dependent on $\frac{a_o}{d}$

#### Understanding the question
 - Absorption coefficient: The change in intensity per unit length is proportional to the intensity, the absorption coefficient is the coeficient of proportionality. $$\frac{dI}{dx}=-aI$$
 - Band pass edge: The quantity that defines the boundaries of the wavelengths that pass. The filter will allow a ray to pass if it's intensity is greater than the band pass edge.
#### Designing an Answer
 - The wavelength at the band pass edge can be given by the equation $$\frac{I_0}{\rho}=I_0a_0\left(1-\frac{\lambda}{\lambda_0}\right)$$
   ![[Jest 2019, Section B, Question 3, fig1.excalidraw]]
 - Since the above equation is linear, we will have only one value for lambda. So the other bound for bandwidth will be the peak intensity.